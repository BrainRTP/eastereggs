# API Документация

Прежде всего нужно подключить EasterEggs.jar к вашему проекту.

Основные манипуляции с пасхалками происходят с помощью сервиса `EasterEggService`

Достать его можно из регистратора сервисов bukkit.

```java
EasterEggService service;

@Override
public void onEnable(){
    service = getServer().getServicesManager().getRegistration(EasterEggService.class).getProvider();
}
```

Сам он содержит в себе такие методы:

`getCategory(String name)` - Получить категорию пасхалок. Возвращает `Optional<EasterEggCategory>`  
`getEasterEgg(Location location)` - Получить пасхалку по локации. Если она отсутствует, вернет `Optional.EMPTY`  
`getAllEggs()` - Получить список всех пасхалок сервера  
`getEggsOfType(String type)` - Возвращает список пасхалок по их типу (BLOCK, ENTITY)  
`createNewCategory(String name)` - Создает новую категорию пасхалок и записывает в файл  
`createNewEgg(String category, EasterEgg egg)` - Создает новою пасхалку в категории.  
`deleteEgg(String category, int id)` - Удаляет пасхалку с номером `<id>` из категории  
`deleteCategory(String category)` - Удаляет категорию пасхалок и данные о ней у всех игроков  
`addEggToPlayer(Player player, EasterEgg egg)` - Добавляет пасхалку игроку, но не активирует ее.  
`reloadAllEggs()` - Перезагружает в память все пасхалки из файлов

## Создание нового действия
Для создания нового действия вы должны создать класс-наследник от класса `Action` из пакета  
`ru.nanit.eastereggs.data.actions`. Данный класс требует переопределения только метода `activate(Player player, Placeholder ph)`.
Здесь player - игрок активировавший пасхалку, а Placeholder - контейнер для передаваемых плейсхолдеров, который содержит стандартные плейсхолдеры.

Важные для указания поля можно пометить аннотацией `@JsonRequired`, тогда если при вводе команды игрок забыл указать нужные данные, ему выкинет ошибку.

Для каждого действия нужно так же создать Serializer, для правильной сериализации его в файл.
Для этого нужно создать класс и реализовать интерфейс `TypeSerializer`. О сериализации в формате HOCON 
можете почитать здесь https://docs.spongepowered.org/stable/ru/plugin/configuration/serialization.html

Когда ваш класс и сериалайзер готовы, их нужно зарегистрировать при помощи сервиса `ActionRegistrator`. 
ССылку на него вы получите из главного сервиса плагина.

Регистриуются действия так:

```java
eggService.getActionRegistrator().registerAction(<name>, TypeToken.of(<ActionClass>.class), new <ActionSerializer>());
```

`<name>` - имя действия, которое в команде указывается в параметре `<action name>`  
`<ActionClass>` - класс действия  
`<ActionSerializer>` - ваш Serializer   

После этого нужно перезагрузить все пасхалки, чтобы сервис запомнил ваши новвоведения

```java
service.reloadAllEggs();
```